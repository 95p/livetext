'use strict';
import {getCurrentPage} from "./script.js";

const limit = 12;
const timeLimit = 650;

let oldX = null;
let oldY = null;
let oldZ = null;
let last = null;

function handleMotionEvent(event) {
    const x = event.accelerationIncludingGravity.x;
    const y = event.accelerationIncludingGravity.y;
    const z = event.accelerationIncludingGravity.z;
    if ([oldX, oldY, oldZ, last].some(e => e === null)) { // no measurement yet
        oldX = x;
        oldY = y;
        oldZ = z;
        last = new Date();
        return;
    }
    const delX = Math.abs(oldX - x);
    const delY = Math.abs(oldY - y);
    const delZ = Math.abs(oldZ - z);
    const deltas = [delX, delY, delZ];
    if (deltas.filter(v => v > limit).length > 1 && (new Date() - last) > timeLimit) {
        last = new Date();
        shakeDetected();
    }
    oldX = x;
    oldY = y;
    oldZ = z;
}

function shakeDetected() {
    console.log('shake detected')
    if (getCurrentPage() === 'wordcloud') {
        document.getElementById('generate').click();
    }
}

window.addEventListener("devicemotion", handleMotionEvent, false);

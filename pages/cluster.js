'use strict';
import {transcriptFrequency} from "./wordcloud.js";

function doListing(common) {
    const el = document.getElementById('most');
    el.innerHTML = '';
    const mostCommon = transcriptFrequency(common).sort((a, b) => (a.size < b.size) ? 1 : a.size > b.size ? -1 : 0);
    console.log(mostCommon);
    mostCommon.forEach(e =>
        el.innerHTML += `<li class="list-group-item d-flex justify-content-between align-items-center">${e.text}<span class="badge badge-primary badge-pill">${e.size}</span></li>`
    );
}

export default function () {
    document.getElementById('doListing').onclick = () => doListing(true);
    document.getElementById('doListingUncommon').onclick = () => doListing(false);
    doListing(true);
}

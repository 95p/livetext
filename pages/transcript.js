'use strict';
import {getCurrentPage} from '../script.js';

let running = false; // speech recognition currently running?
let cleared = false; // false: button has state 'clear', true: button has state 'undo'
function handleStart() {
    const el = document.getElementById('start');
    if (running) {
        el.innerText = 'Start';
        spr.stop();
        let clearEl = document.getElementById('clear');
        clearEl.style.display = 'inline';
    } else {
        el.innerText = 'Stopp';
        speechRecognition();
        resetClear();
    }
    running = !running;
}

function resetClear() {
    let clearEl = document.getElementById('clear');
    clearEl.style.display = 'none';
    cleared = false;
}

function handleClear() {
    let el = document.getElementById('clear');
    if (!cleared) {
        el.innerText = 'undo';
        clear();
    } else {
        el.innerText = 'clear';
        undo();
    }
    cleared = !cleared;
}

let oldTranscript;

function clear() {
    oldTranscript = finalTranscript;
    setFinalTranscript(' ');
}

function undo() {
    setFinalTranscript(oldTranscript);
}

let spr = null;
let finalTranscript = ' ';

export function getFinalTranscript() {
    return finalTranscript;
}

function setFinalTranscript(t) {
    finalTranscript = t;
    if (getCurrentPage() === 'transcript') {
        document.getElementById('final_text').innerText = finalTranscript;
    }
}

function appendFinalTranscript(a) {
    finalTranscript += a;
    if (getCurrentPage() === 'transcript') {
        document.getElementById('final_text').innerText += ' ' + a;
    }
}

const LANG = 'de-DE';

function speechRecognition() {
    spr = new webkitSpeechRecognition();
    spr.continuous = true; // set to true to disable activation when speaking
    spr.interimResults = true; // to get later but better results set to false
    spr.lang = LANG;
    spr.onstart = function () {
        console.log('SPR', 'Recognition started');
    };
    spr.onresult = function (event) {
        let interim_transcript = '';
        for (let i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                appendFinalTranscript(event.results[i][0].transcript);
            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }
        if (getCurrentPage() === 'transcript') {
            document.getElementById('interim_text').innerHTML = interim_transcript;
        }
    };
    spr.onerror = function (event) {
        console.error(event);
    };
    spr.onend = function () {
        console.log('terminated');
    };
    spr.start();
}

export default function () {
    document.getElementById('start').onclick = () => handleStart();``
    document.getElementById('clear').onclick = () => handleClear();
    document.getElementById('final_text').innerText = finalTranscript;
}
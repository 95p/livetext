'use strict';
import {getFinalTranscript} from "./transcript.js";
import {commonWords} from "../words.js";

let layout;
function handleGenerate(common) {
    const width = document.getElementById('wordcloud-box').offsetWidth;
    const height = document.getElementById('wordcloud-box').offsetHeight;
    const words = transcriptFrequency(common);
    layout = d3.layout.cloud()
        .size([width, height])
        .words(words.map(e => ({text: e.text, size: e.size / words.length * width})))
        .padding(5)
        .rotate(() => ~~(Math.random() * 2) * 90)
        .font('Roboto')
        .fontSize(d => d.size)
        .on('end', draw);
    layout.start();
}

function frequency(arr) {
    const frequency = [];
    arr.forEach(e => {
        const index = frequency.findIndex(l => l.text === e)
        if (index < 0) {
            frequency.push({text: e, size: 1});
        } else {
            frequency[index].size += 1;
        }
    });
    return frequency;
}

/**
 * Get the number of occurrences for each word in the transcript
 * @param common true if stop words (common words) should be included
 * @returns {[]} array with object of the form {text, size}
 */
export function transcriptFrequency(common) {
    const text = getFinalTranscript().replace(/[.,\/#!$%^&*;:{}=\-_`~()]/g, "");
    const words = text.split(' ').filter(e => common ? e !== '' : !(commonWords.includes(e)));
    return frequency(words);
}

function draw(words) {
    d3.selectAll('svg').remove();
    d3.select('#wordcloud-box')
        .append('svg')
        .attr('width', layout.size()[0])
        .attr('height', layout.size()[1])
        .append('g')
        .attr('transform', 'translate(' + layout.size()[0] / 2 + ',' + layout.size()[1] / 2 + ')')
        .selectAll('text')
        .data(words)
        .enter().append('text')
        .transition()
        .duration(400)
        .style('fill-opacity', 1)
        .style('font-size', d => d.size + 'px')
        .style('font-family', 'Roboto')
        .attr('text-anchor', 'middle')
        .attr('transform', d => 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')')
        .text(d => d.text)
}

export default function () {
    document.getElementById('generate').onclick = () => handleGenerate(true);
    document.getElementById('generateUncommon').onclick = () => handleGenerate(false);
    handleGenerate();
}
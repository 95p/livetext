'use strict';

// https://github.com/spencermountain/compromise/
// https://github.com/winkjs/wink-nlp-utils

const pages = ['transcript', 'wordcloud', 'cluster', 'imprint', 'policy'];
let currentPage = pages[0];

export function getCurrentPage() {
    return currentPage;
}

window.onload = function (e) {
    const startPage = pages[0];
    loadTab(startPage);
    document.getElementById('header').onclick = () => { // make header return to startPage
        loadTab(startPage);
        document.getElementById(currentPage).classList.remove('active');
        document.getElementById(startPage).classList.add('active');
        currentPage = startPage;
    };
    for (const page of pages) {
        const element = document.getElementById(page)
        element.onclick = () => {
            loadTab(page);
            document.getElementById(currentPage).classList.remove('active');
            element.classList.add('active');
            currentPage = page;
        };
    }
    if (!('webkitSpeechRecognition' in window)) {
        displayNotSupported();
    }
}

function loadTab(page) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'pages/' + page + '.html', true);
    xhr.onreadystatechange = function () {
        if (this.readyState !== 4) return;
        if (this.status !== 200) return;
        document.getElementById('app').innerHTML = this.responseText;
        import('./pages/' + page + '.js')
            .then(module => module.default())
            .catch(e => console.error(e));
    };
    xhr.send();
}

function displayNotSupported() {
    const warning = `<div class="alert alert-danger" role="alert">
        Dieser Browser unterstützt keine Spracherkennung. Lade <a href="https://www.google.com/intl/de_de/chrome/" class="alert-link">Chrome</a> herunter.
                     </div>`;
    document.getElementById('content').insertAdjacentHTML('afterbegin', warning);
}